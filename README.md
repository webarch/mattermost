# Webarchitects Mattermost Ansible Role

An Ansible role to install and remove Mattermost on Debian, see the [mattermost-server](https://git.coop/webarch/mattermost-server) repo for an example of how to use this role to provision a Mattermost server.

References:

* [Installing Mattermost on Debian Buster](https://docs.mattermost.com/install/install-debian.html)
